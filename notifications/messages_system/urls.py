from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from .api import *


router = DefaultRouter()
# router.register(r'messages_system', MessagesViewSet, base_name="message")


urlpatterns = router.urls
urlpatterns.append(url(r'write_message/', write_message))
urlpatterns.append(url(r'get_messages/', get_messages))
urlpatterns.append(url(r'read_message/', read_message))
urlpatterns.append(url(r'delete_message/', delete_message))
urlpatterns.append(url(r'get_unread_messages/', get_unread_messages))
