from django.http import JsonResponse, HttpResponse, HttpResponseForbidden
from rest_framework.decorators import api_view
from messages_system.utils import get_user_from_request
from .models import Message
from django.db.models import Q
from django.forms.models import model_to_dict


@api_view(['POST'])
def write_message(request):
    if request.user.is_authenticated:
        sender = get_user_from_request(request)
        receiver_id = request.data.get('receiver_id', '')
        subject = request.data.get('subject', '')
        message = request.data.get('message', '')
        new_message = Message.objects.create(sender=sender, receiver_id=receiver_id,
                                             subject=subject, message=message)
        return JsonResponse({"new_message": model_to_dict(new_message)}, safe=False, status=201)
    return HttpResponseForbidden()


@api_view(['GET'])
def get_messages(request):
    if request.user.is_authenticated:
        data = list(Message.objects.filter(Q(sender=request.user) | Q(receiver=request.user)).values())
        return JsonResponse(data, safe=False)
    return HttpResponseForbidden()


@api_view(['GET'])
def get_unread_messages(request):
    if request.user.is_authenticated:
        data = list(Message.objects.filter(Q(sender=request.user) | Q(receiver=request.user),already_read=False).values())
        return JsonResponse(data, safe=False)
    return HttpResponseForbidden()


@api_view(['GET'])
def read_message(request):
    if request.user.is_authenticated:
        try:
            requested_message_id = request.GET.get("message_id")
            requested_message = Message.objects.get(id=requested_message_id, receiver=request.user)
            requested_message.already_read = 1
            requested_message.save()
            return JsonResponse({"requested_message": model_to_dict(requested_message)}, safe=False, status=200)
        except Message.DoesNotExist:
            return HttpResponse(status=404)
    return HttpResponseForbidden()


@api_view(['DELETE'])
def delete_message(request):
    if request.user.is_authenticated:
        try:
            message_id = request.GET.get("message_id")
            message_to_delete = Message.objects.get(Q(sender=request.user) | Q(receiver=request.user), id=message_id)
            message_to_delete.delete()
            return HttpResponse(status=204)
        except Message.DoesNotExist:
            return HttpResponse(status=404)
    return HttpResponseForbidden()
